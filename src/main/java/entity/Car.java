package entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;


@Entity(name = "CAR")
public class Car implements Serializable {

	private static final long serialVersionUID = -5378014308697582413L;

	@Id
	@Column(name = "car_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int carId;

	@Column(name = "car_number", unique = true)
	private String carNumber;

	@Column(name = "car_model")
	private String carModel;

	@Column(name = "car_colour")
	private String colour;

	@Column(name = "car_description")
	private String description;

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "PERSON_CAR", joinColumns = { @JoinColumn(name = "car_id") }, inverseJoinColumns = {
			@JoinColumn(name = "person_id") })
	private Set<Person> people;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "car_id", referencedColumnName = "car_id")
	private Set<Order> orders;

		public Car(int carId, String carNumber, String carModel, String colour, String description, Set<Person> personSet,
			Set<Order> orderSet) {
		super();
		this.carId = carId;
		this.carNumber = carNumber;
		this.carModel = carModel;
		this.colour = colour;
		this.description = description;
		this.people = personSet;
		this.orders = orderSet;
	}

	public Car() {

	}

	public int getCarId() {
		return carId;
	}

	public void setCarId(int carId) {
		this.carId = carId;
	}

	public String getCarNumber() {
		return carNumber;
	}

	public void setCarNumber(String carNumber) {
		this.carNumber = carNumber;
	}

	public String getCarModel() {
		return carModel;
	}

	public void setCarModel(String carModel) {
		this.carModel = carModel;
	}

	public String getColour() {
		return colour;
	}

	public void setColour(String colour) {
		this.colour = colour;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Set<Person> getPersonSet() {
		return people;
	}

	public void setPersonSet(Set<Person> personSet) {
		this.people = personSet;
	}

	public Set<Order> getOrderSet() {
		return orders;
	}

	public void setOrderSet(Set<Order> orders) {
		this.orders = orders;
	}

	@Override
	public String toString() {
		return "Car [carId=" + carId + ", carNumber=" + carNumber + ", carModel=" + carModel + ", colour=" + colour
				+ ", description=" + description + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + carId;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Car other = (Car) obj;
		if (carId != other.carId)
			return false;
		return true;
	}

	

	

}
