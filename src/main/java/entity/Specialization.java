package entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

@Entity(name = "SPECIALIZATION")
public class Specialization implements Serializable{

	private static final long serialVersionUID = 6252250883396973173L;
	
	@Id
	@Column(name = "specialization_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int specializationId;
	@Column(name = "specialization")
	private String specialization;
	@Column(name = "specialization_comments")
	private String specializationComments;
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "REPAIRER_SPECIALIZATION", joinColumns = {
			@JoinColumn(name = "specialization_id") }, inverseJoinColumns = { @JoinColumn(name = "person_id") })
	private Set<Person> people;
	
	
	public Specialization(String specialization, String specializationComments, Set<Person> personSet) {
		super();
		this.specialization = specialization;
		this.specializationComments = specializationComments;
		this.people = personSet;
	}
	public Specialization() {
		
	}
	public String getSpecialization() {
		return specialization;
	}
	public void setSpecialization(String specialization) {
		this.specialization = specialization;
	}
	public String getSpecializationComments() {
		return specializationComments;
	}
	public void setSpecializationComments(String specializationComments) {
		this.specializationComments = specializationComments;
	}
	public Set<Person> getPersonSet() {
		return people;
	}
	public void setPersonList(Set<Person> people) {
		this.people = people;
	}
	public int getSpecializationId() {
		return specializationId;
	}
	public void setSpecializationId(int specializationId) {
		this.specializationId = specializationId;
	}
	@Override
	public String toString() {
		return "Specialization [specializationId=" + specializationId + ", specialization=" + specialization
				+ ", specializationComments=" + specializationComments + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + specializationId;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Specialization other = (Specialization) obj;
		if (specializationId != other.specializationId)
			return false;
		return true;
	}
	
		

}
