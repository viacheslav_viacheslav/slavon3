package entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.CreationTimestamp;

@Entity(name = "COMMENT")
public class Comment implements Serializable {

	private static final long serialVersionUID = 3492364361103581399L;

	@Id
	@Column(name = "comment_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int commentId;
	@ManyToOne
	@JoinColumn(name = "person_id")
	private Person person;
	@Column(name = "comment_date_time")
	@CreationTimestamp
	private Timestamp commentDateTime;
	@Column(name = "comment_text")
	private String commentText;
	@ManyToOne
	@JoinColumn(name = "order_id")
	private Order order;

	public Comment(Person person, Timestamp commentDateTime, String commentText, Order order) {
		super();
		this.person = person;
		this.commentDateTime = commentDateTime;
		this.commentText = commentText;
		this.order = order;
	}

	public Comment() {
		
	}

	public int getCommentId() {
		return commentId;
	}

	public void setCommentId(int commentId) {
		this.commentId = commentId;
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	public Timestamp getCommentDateTime() {
		return commentDateTime;
	}

	public void setCommentDateTime(Timestamp commentDateTime) {
		this.commentDateTime = commentDateTime;
	}

	public String getCommentText() {
		return commentText;
	}

	public void setCommentText(String commentText) {
		this.commentText = commentText;
	}

	public Order getOrder() {
		return order;
	}

	public void setOrderId(Order order) {
		this.order = order;
	}

	@Override
	public String toString() {
		return "Comment [commentId=" + commentId + ", person=" + person + ", commentDateTime=" + commentDateTime
				+ ", commentText=" + commentText + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + commentId;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Comment other = (Comment) obj;
		if (commentId != other.commentId)
			return false;
		return true;
	}

	
}
