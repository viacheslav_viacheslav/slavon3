package entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;


@Entity(name = "ORDER_STATUS")
public class OrderStatus implements Serializable {

	private static final long serialVersionUID = -2238783350268317541L;

	@Id
	@Column(name = "order_status_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int orderStatusId;
	@Column(name = "order_status")
	private String orderStatus;
	@OneToMany (cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn (name = "order_status_id", referencedColumnName = "order_status_id")	
	private Set<Order> orders;

	public OrderStatus(String orderStatus, Set<Order> orderSet) {
		super();
		this.orderStatus = orderStatus;
		this.orders = orderSet;
	}

	public OrderStatus() {

	}

	public int getOrderStatusId() {
		return orderStatusId;
	}

	public void setOrderStatusId(int orderStatusId) {
		this.orderStatusId = orderStatusId;
	}

	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	public Set<Order> getOrderSet() {
		return orders;
	}

	public void setOrderSet(Set<Order> orders) {
		this.orders = orders;
	}

	@Override
	public String toString() {
		return "OrderStatus [orderStatusId=" + orderStatusId + ", orderStatus=" + orderStatus +"]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + orderStatusId;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OrderStatus other = (OrderStatus) obj;
		if (orderStatusId != other.orderStatusId)
			return false;
		return true;
	}
}
