package entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;


@Entity(name = "PERSON")
public class Person implements Serializable {

	private static final long serialVersionUID = -8258628307595580629L;

	@Id
	@Column(name = "person_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int personId;
	@Column(name = "person_first_name", nullable = false)
	private String firstName;
	@Column(name = "person_patronymic")
	private String patronymic;
	@Column(name = "person_last_name")
	private String personLastName;
	@Column(name = "person_email")
	private String email;
	@Column(name = "person_telephone_number")
	private String telephoneNumber;
	@Column(name = "person_login", unique = true, nullable = false)
	private String login;
	@Column(name = "person_password")
	private String password;
	@Column(name = "person_status", columnDefinition = "VARCHAR(20) default 'ACTIVE'")
	private String status;
	@ManyToOne
	@JoinColumn(name = "person_role_id")
	private Role role;
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "REPAIRER_SPECIALIZATION", joinColumns = {
			@JoinColumn(name = "person_id") }, inverseJoinColumns = { @JoinColumn(name = "specialization_id") })
	private Set<Specialization> specializations;
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "ORDER_PERSON", joinColumns = {
			@JoinColumn(name = "person_id",nullable=true) }, inverseJoinColumns = { @JoinColumn(name = "order_id") })
	private Set<Order> orders;
	@OneToMany (cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn (name = "person_id", referencedColumnName = "person_id")
	private Set<Comment> comments;
	 @Transient
	 private Set<Integer> roles;
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "PERSON_CAR", joinColumns = {
			@JoinColumn(name = "person_id") }, inverseJoinColumns = { @JoinColumn(name = "car_id") })
	private Set<Car> cars;

	public Person(String firstName, String personPatronymic, String personLastName, String personEmail,
			String personTelephoneNumber, String personLogin, String personPassword,
			Set<Specialization> specializationSet, Set<Order> orderSet, Set<Comment> commentSet,
			Set<Integer> roleSet, Set<Car> carSet) {
		super();

		this.firstName = firstName;
		this.patronymic = personPatronymic;
		this.personLastName = personLastName;
		this.email = personEmail;
		this.telephoneNumber = personTelephoneNumber;
		this.login = personLogin;
		this.password = personPassword;
		this.specializations = specializationSet;
		this.orders = orderSet;
		this.comments = commentSet;
		this.roles = roleSet;
		this.cars = carSet;
	}

	public Person() {
	}

	public int getPersonId() {
		return personId;
	}

	public void setPersonId(int personId) {
		this.personId = personId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getPersonPatronymic() {
		return patronymic;
	}

	public void setPersonPatronymic(String personPatronymic) {
		this.patronymic = personPatronymic;
	}

	public String getPersonLastName() {
		return personLastName;
	}

	public void setPersonLastName(String personLastName) {
		this.personLastName = personLastName;
	}

	public String getPersonEmail() {
		return email;
	}

	public void setPersonEmail(String personEmail) {
		this.email = personEmail;
	}

	public String getPersonTelephoneNumber() {
		return telephoneNumber;
	}

	public void setPersonTelephoneNumber(String personTelephoneNumber) {
		this.telephoneNumber = personTelephoneNumber;
	}

	public String getPersonLogin() {
		return login;
	}

	public void setPersonLogin(String personLogin) {
		this.login = personLogin;
	}

	public String getPersonPassword() {
		return password;
	}

	public void setPersonPassword(String personPassword) {
		this.password = personPassword;
	}

	public Set<Specialization> getSpecializationSet() {
		return specializations;
	}

	public void setSpecializationSet(Set<Specialization> specializations) {
		this.specializations = specializations;
	}

	public Set<Order> getOrderSet() {
		return orders;
	}

	public void setOrderSet(Set<Order> orders) {
		this.orders = orders;
	}

	public Set<Comment> getCommentSet() {
		return comments;
	}

	public void setCommentSet(Set<Comment> comments) {
		this.comments = comments;
	}

	 public Set<Integer> getRoleSet() {
	 return roles;
	 }
	
	 public void setRoleSet(Set<Integer> roles) {
	 this.roles = roles;
	 }

	public Set<Car> getCarSet() {
		return cars;
	}

	public void setCarSet(Set<Car> cars) {
		this.cars = cars;
	}

	@Override
	public String toString() {
		return "Person [personId=" + personId + ", firstName=" + firstName + ", personPatronymic=" + patronymic
				+ ", personLastName=" + personLastName + ", personEmail=" + email + ", personTelephoneNumber="
				+ telephoneNumber + ", personLogin=" + login + ", personPassword=" + password
				+ "]";
	}

	public String getPersonStatus() {
		return status;
	}

	public void setPersonStatus(String personStatus) {
		this.status = personStatus;
	}

	public Role getPersonRole() {
		return role;
	}

	public void setPersonRole(Role personRole) {
		this.role = personRole;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + personId;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Person other = (Person) obj;
		if (personId != other.personId)
			return false;
		return true;
	}


	
	
}
