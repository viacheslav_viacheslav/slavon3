package entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.faces.bean.SessionScoped;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

@Entity(name="ORDERS")
public class Order implements Serializable {

	private static final long serialVersionUID = 8004108419814664371L;

	@Id
	@Column(name="order_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int orderId;
	@ManyToOne
	@JoinColumn(name="car_id")
	private Car car;
	@ManyToOne 
	@JoinColumn(name="order_status_id")
	private OrderStatus orderStatus;
	@Column(name="order_price")
	private float orderPrice;
	@Transient
	private int clientId;
	@Transient
	private int repairerId;
	@Transient
	private Comment lastComment;
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "ORDER_PERSON", joinColumns = {
			@JoinColumn(name = "order_id") }, inverseJoinColumns = { @JoinColumn(name = "person_id",nullable=true) })
	private Set<Person> people;
	@OneToMany (cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
	@JoinColumn (name = "order_id", referencedColumnName = "order_id")
	private List<Comment> commentList;
    @Transient
    boolean editable;
	public Order(int orderId, Car car, OrderStatus orderStatus, float orderPrice, Set<Person> personSet,
			List<Comment> commentList) {
		super();
		this.orderId = orderId;
		this.car = car;
		this.orderStatus = orderStatus;
		this.orderPrice = orderPrice;
		this.people = personSet;
		this.commentList = commentList;
	}

	public Order() {

	}

	public int getOrderId() {
		return orderId;
	}

	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}

	public Car getCar() {
		return car;
	}

	public void setCar(Car car) {
		this.car = car;
	}

	public OrderStatus getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(OrderStatus orderStatus) {
		this.orderStatus = orderStatus;
	}

	public float getOrderPrice() {
		return orderPrice;
	}

	public void setOrderPrice(float orderPrice) {
		this.orderPrice = orderPrice;
	}

	public Set<Person> getPersonSet() {
		return people;
	}

	public void setPersonSet(Set<Person> people) {
		this.people = people;
	}

	public List<Comment> getCommentList() {
		return commentList;
	}

	public void setCommentList(List<Comment> comments) {
		this.commentList = comments;
	}

	public int getClientId() {
		return clientId;
	}

	public void setClientId(int clientId) {
		this.clientId = clientId;
	}

	public int getRepairerId() {
		return repairerId;
	}

	public void setRepairerId(int repairerId) {
		this.repairerId = repairerId;
	}

	public boolean isEditable() {
		return editable;
	}

	public void setEditable(boolean editable) {
		this.editable = editable;
	}

	@Override
	public String toString() {
		return "Order [orderId=" + orderId + ", car=" + car + ", orderStatus=" + orderStatus + ", orderPrice="
				+ orderPrice + ", clientId=" + clientId + ", repairerId=" + repairerId + "]";
	}

	public Comment getLastComment() {
	
		return this.commentList.get(commentList.size()-1);
		
	}

	public void setLastComment(Comment lastComment) {
		this.lastComment = lastComment;
		if(commentList ==null){
			commentList =new ArrayList<>();
		}
		this.commentList.add(lastComment);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + orderId;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Order other = (Order) obj;
		if (orderId != other.orderId)
			return false;
		return true;
	}



}
