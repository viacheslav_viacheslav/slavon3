package service;

import java.util.List;
import dao.SpecializationDao;
import entity.Specialization;

import javax.faces.bean.ManagedBean;

//@ManagedBean(name = "specializationService")
public class SpecializationService {
	private SpecializationDao specializationDao;

	public SpecializationDao getSpecializationDao() {
		return specializationDao;
	}

	public void setSpecializationDao(SpecializationDao specializationDao) {
		this.specializationDao = specializationDao;
	}

	public void insertOrUpdateSpecialization(Specialization specialization) {
		specializationDao.insertOrUpdateSpecialization(specialization);
	}

	public Specialization getSpecializationById(int id) {
		return specializationDao.getSpecializationById(id);
	}

	public void deleteSpecialization(Specialization specialization) {
		specializationDao.deleteSpecialization(specialization);
	}

	public List<Specialization> getAllSpecialization() {
		return specializationDao.getAllSpecialization();
	}
}
