package service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import dao.CommentDao;
import entity.Comment;

import javax.faces.bean.ManagedBean;

//@ManagedBean(name = "commentService")
public class CommentService {
	private CommentDao commentDao;

	public CommentDao getCommentDao() {
		return commentDao;
	}

	public void setCommentDao(CommentDao commentDao) {
		this.commentDao = commentDao;
	}

	public void insertComment(Comment comment) {
		commentDao.insertComment(comment);
	}

	public Comment getCommentById(int id) {
		return commentDao.getCommentById(id);
	}

	public List<Comment> getAllComments() {
		return commentDao.getAllComments();
	}
	
	public Map<Integer, Comment> getCommentMap(){
		List<Comment> allCommentsList = commentDao.getAllComments();
		Map<Integer, Comment> allCommentsMap = new HashMap<>();
		for(Comment comment : allCommentsList){
			allCommentsMap.put(comment.getCommentId(), comment);
		}
		return allCommentsMap;
		
	}
}
