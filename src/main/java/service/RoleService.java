package service;

import java.util.List;
import dao.RoleDao;
import entity.Role;

import javax.faces.bean.ManagedBean;

//@ManagedBean(name = "roleService")
public class RoleService {
	private RoleDao roleDao;

	public RoleDao getRoleDao() {
		return roleDao;
	}

	public void setRoleDao(RoleDao roleDao) {
		this.roleDao = roleDao;
	}

	public void insertOrUpdateRole(Role role) {
		roleDao.insertOrUpdateRole(role);
	}

	public Role getRoleById(int id) {
		return roleDao.getRoleById(id);
	}

	public void deleteRole(Role role) {
		roleDao.deleteRole(role);
	}

	public List<Role> getAllRoles() {
		return roleDao.getAllRoles();

	}
}
