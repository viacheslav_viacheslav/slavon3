package service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import dao.PersonDao;
import entity.Person;

public class MyUserDetailsService implements UserDetailsService {

	private PersonDao personDao;
	
	public PersonDao getPersonDao() {
		return personDao;
	}

	public void setPersonDao(PersonDao personDao) {
		this.personDao = personDao;
	}

	@Override
	public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException, DataAccessException {
		Person person = personDao.getPersonByLogin(login);
		
		Set<GrantedAuthority> setAuths = new HashSet<GrantedAuthority>();
		setAuths.add(new SimpleGrantedAuthority(person.getPersonRole().getRoleName()));
		List<GrantedAuthority> resultAuths = new ArrayList<GrantedAuthority>(setAuths);

		User user = new User(person.getPersonLogin(), person.getPersonPassword(),
				(!"INACTIVE".equals(person.getPersonStatus())) ? true : false, true, true, true, resultAuths);
		return user;
	}
	



}
