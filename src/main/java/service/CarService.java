package service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import dao.CarDao;
import entity.Car;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;

//@ManagedBean(name = "carService")
public class CarService {

	
	private CarDao carDao;

	public CarDao getCarDao() {
		return carDao;
	}

	public void setCarDao(CarDao carDao) {
		this.carDao = carDao;
	}

	public void insertOrUpdateCar(Car car) {
		carDao.insertOrUpdateCar(car);
	}

	public Car getCarById(int id) {
		return carDao.getCarById(id);
	}

	public void deleteCar(Car car) {
		carDao.deleteCar(car);
	}

	public List<Car> getAllCars() {
		return carDao.getAllCars();
	}
	
	public Map<Integer,Car>  getCarMap(){
		List<Car> carList = carDao.getAllCars();
		Map<Integer,Car> carMap = new HashMap<>();
		for(Car car : carList){
			carMap.put(car.getCarId(), car);
		}		
		return carMap;
		
	}

}
