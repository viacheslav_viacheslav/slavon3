package service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import dao.OrderStatusDao;
import entity.OrderStatus;

import javax.faces.bean.ManagedBean;

//@ManagedBean(name = "orderStatusService")
public class OrderStatusService {
	
	private OrderStatusDao orderStatusDao;

	public OrderStatusDao getOrderStatusDao() {
		return orderStatusDao;
	}

	public void setOrderStatusDao(OrderStatusDao orderStatusDao) {
		this.orderStatusDao = orderStatusDao;
	}

	public void insertOrUpdateOrderStatus(OrderStatus orderStatus) {
		orderStatusDao.insertOrUpdateOrderStatus(orderStatus);
	}

	public OrderStatus getOrderStatusById(int id) {
		return orderStatusDao.getOrderStatusById(id);
	}

	public void deleteOrderStatus(OrderStatus orderStatus) {
		orderStatusDao.deleteOrderStatus(orderStatus);
	}

	public List<OrderStatus> getAllOrderStatuses() {
		return orderStatusDao.getAllOrderStatuses();
	}

	public Map<Integer, OrderStatus> gerOrderStatusMap() {
		List<OrderStatus> orderStatusList = orderStatusDao.getAllOrderStatuses();
		Map<Integer, OrderStatus> orderStatusMap = new HashMap<>();
		for (OrderStatus orderStatus : orderStatusList) {
			orderStatusMap.put(orderStatus.getOrderStatusId(), orderStatus);
		}
		return orderStatusMap;

	}
}
