package service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import dao.PersonDao;
import entity.Person;

import javax.faces.bean.ManagedBean;

//@ManagedBean(name = "personService")
public class PersonService {

	private PersonService personService;

	public PersonService getPersonService() {
		return personService;
	}

	public void setPersonService(PersonService personService) {
		this.personService = personService;
	}

	private RoleService roleService;

	public RoleService getRoleService() {
		return roleService;
	}

	public void setRoleService(RoleService roleService) {
		this.roleService = roleService;
	}

	private PersonDao personDao;

	public PersonDao getPersonDao() {
		return personDao;
	}

	public void setPersonDao(PersonDao personDao) {
		this.personDao = personDao;
	}

	public void insertOrUpdatePerson(Person person) {
		personDao.insertOrUpdatePerson(person);
	}

	public Person getPersonById(int id) {
		return personDao.getPersonById(id);
	}

	public Person getPersonByLogin(String login) {
		return personDao.getPersonByLogin(login);
	}

	public void deletePerson(Person person) {
		person.setPersonStatus("INACTIVE");
		personDao.insertOrUpdatePerson(person);
	}

	public List<Person> getAllPeople() {
		return personDao.getAllPeople();
	}

	public Map<Integer, Person> getPersonMap() {
		List<Person> personList = personService.getAllPeople();
		Map<Integer, Person> personMap = new HashMap<>();
		for (Person person : personList) {
			personMap.put(person.getPersonId(), person);
		}
		return personMap;

	}

	public List<Person> getClients() {
		return personDao.getClients();
	}

	public List<Person> getRepairers() {
		return personDao.getRepairers();
	}

	public void saveOrUpdateUser(int personId, String person_name, String person_patronymic, String person_last_name,
			String person_email, String person_telephone_number, String person_login, String person_password,
			String person_role) {
		Person person = null;
		if(personId==0){
			person = new Person();
		}else {
			person = personService.getPersonById(personId);
		}
		person.setFirstName(person_name);
		person.setPersonLastName(person_last_name);
		person.setPersonPatronymic(person_patronymic);
		person.setPersonEmail(person_email);
		person.setPersonTelephoneNumber(person_telephone_number);
		person.setPersonLogin(person_login);
		person.setPersonPassword(person_password);
		int roleId = Integer.parseInt(person_role);
		person.setPersonRole(roleService.getRoleById(roleId));
		personService.insertOrUpdatePerson(person);
	}

}
