package service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestParam;

import dao.CommentDao;
import dao.OrderDao;
import dao.PersonDao;
import entity.Comment;
import entity.Order;
import entity.Person;

import javax.faces.bean.ManagedBean;

//@ManagedBean(name = "orderService")
public class OrderService {
	private OrderDao orderDao;

	public OrderDao getOrderDao() {
		return orderDao;
	}

	public void setOrderDao(OrderDao orderDao) {
		this.orderDao = orderDao;
	}

	private PersonDao personDao;

	public PersonDao getPersonDao() {
		return personDao;
	}

	public void setPersonDao(PersonDao personDao) {
		this.personDao = personDao;
	}

	private CommentDao commentDao;

	public CommentDao getCommentDao() {
		return commentDao;
	}

	public void setCommentDao(CommentDao commentDao) {
		this.commentDao = commentDao;
	}

	private PersonService personService;

	public PersonService getPersonService() {
		return personService;
	}

	public void setPersonService(PersonService personService) {
		this.personService = personService;
	}

	private CarService carService;

	public CarService getCarService() {
		return carService;
	}

	public void setCarService(CarService carService) {
		this.carService = carService;
	}

	private OrderStatusService orderStatusService;

	public OrderStatusService getOrderStatusService() {
		return orderStatusService;
	}

	public void setOrderStatusService(OrderStatusService orderStatusService) {
		this.orderStatusService = orderStatusService;
	}

	private CommentService commentService;

	public CommentService getCommentService() {
		return commentService;
	}

	public void setCommentService(CommentService commentService) {
		this.commentService = commentService;
	}

	private OrderService orderService;

	public OrderService getOrderService() {
		return orderService;
	}

	public void setOrderService(OrderService orderService) {
		this.orderService = orderService;
	}

	public void insertOrUpdateOrder(Order order) {
		Set<Person> people = new HashSet<>();
		int clientId = order.getClientId();
		int repairerId = order.getRepairerId();
		if (clientId != 0) {
			people.add(personDao.getPersonById(order.getClientId()));
		}
		if (repairerId != 0) {
			people.add(personDao.getPersonById(order.getRepairerId()));
		}
		if (people.size() != 0) {
			order.setPersonSet(people);
		}
		Comment comment = order.getLastComment();
		int commentId = commentDao.insertComment(comment);
		order.setLastComment(commentDao.getCommentById(commentId));
		orderDao.insertOrUpdateOrder(order);
	}

	public Order getOrderById(int id) {
		Order order = orderDao.getOrderById(id);
		Set<Person> personSet = order.getPersonSet();
		return setPeopleForOrder(order, personSet);
	}

	public void deleteOrder(Order order) {
		orderDao.deleteOrder(order);
	}

	public List<Order> getAllOrders() {
		List<Order> orderList = orderDao.getAllOrders();
		List<Order> newOrderList = new ArrayList<>();
		for (Order order : orderList) {
			Set<Person> personSet = order.getPersonSet();
			newOrderList.add(setPeopleForOrder(order, personSet));
		}
		return newOrderList;
	}

	Order setPeopleForOrder(Order order, Set<Person> personSet) {
		for (Person person : personSet) {
			int persId = person.getPersonId();
			if ((persId != 0) && "ROLE_CLIENT".equals(personDao.getPersonById(persId).getPersonRole().getRoleName())) {
				order.setClientId(persId);
			} else if ((persId != 0)
					&& "ROLE_REPAIRER".equals(personDao.getPersonById(persId).getPersonRole().getRoleName())) {
				order.setRepairerId(persId);
			}
		}
		return order;
	}

	public void createOrUpdateOrder(int orderId, int car_id, String client_id, int order_status_id, String repairer_id,
			String order_price, String order_comment, String submitPerson) {
		Order order = null;
		if (orderId == 0) {
			order = new Order();
		} else {
			order = orderService.getOrderById(orderId);
		}

		order.setCar(carService.getCarById(car_id));
		String clientString = client_id;
		if (clientString.length() != 0) {
			order.setClientId(Integer.parseInt(clientString));
		}
		order.setOrderStatus(orderStatusService.getOrderStatusById(order_status_id));
		String repairerString = repairer_id;
		if (repairerString.length() != 0) {
			order.setRepairerId(Integer.parseInt(repairerString));
		}
		String priceString = order_price;
		if (!"".equals(priceString)) {
			order.setOrderPrice(Float.parseFloat(priceString));
		}
		Comment comment = new Comment();
		comment.setCommentText(order_comment);
		comment.setPerson(personService.getPersonByLogin(submitPerson));

		if (order.getOrderId() != 0) {
			comment.setOrderId(order);
		}
		order.setLastComment(comment);
		orderService.insertOrUpdateOrder(order);

	}

}
