package controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import entity.Person;
import entity.Role;
import service.PersonService;
import service.RoleService;

@Controller
public class UserListConroller {
	@Autowired
	private PersonService personService;

	public PersonService getPersonService() {
		return personService;
	}

	public void setPersonService(PersonService personService) {
		this.personService = personService;
	}

	@Autowired
	private RoleService roleService;

	public RoleService getRoleService() {
		return roleService;
	}

	public void setRoleService(RoleService roleService) {
		this.roleService = roleService;
	}

	@RequestMapping("/users")
	public ModelAndView userList() {
		ModelAndView model = new ModelAndView();
		List<Person> personList = personService.getAllPeople();
		model.addObject("personList", personList);
		model.setViewName("userlistpage");
		return model;
	}

	@RequestMapping("/add_user")
	public ModelAndView addUser() {
		ModelAndView model = new ModelAndView();
		List<Role> roleList = roleService.getAllRoles();
		model.addObject("roleList", roleList);
		model.setViewName("adduserpage");
		return model;
	}

	@RequestMapping("/cancel_user_update")
	public ModelAndView cancelUser() {
		ModelAndView model = new ModelAndView();
		List<Person> personList = personService.getAllPeople();
		model.addObject("personList", personList);
		model.setViewName("userlistpage");
		return model;
	}

	@RequestMapping("/confirm_user_add")
	public ModelAndView confirmAddUser(@RequestParam String person_name, @RequestParam String person_patronymic,
			@RequestParam String person_last_name, @RequestParam String person_email,
			@RequestParam String person_telephone_number, @RequestParam String person_login,
			@RequestParam String person_password, @RequestParam String person_role) {
		
		personService.saveOrUpdateUser(0, person_name, person_patronymic, person_last_name, person_email,
				person_telephone_number, person_login, person_password, person_role);
		return userList();
	}

	@RequestMapping("/delete_user")
	public ModelAndView deleteUser(@RequestParam String userToDelete) {
		Person person = personService.getPersonById(Integer.parseInt(userToDelete));
		personService.deletePerson(person);
		return userList();
	}

	@RequestMapping("/update_user")
	public ModelAndView updateUser(@RequestParam String userToUpdate) {
		ModelAndView model = new ModelAndView();
		Person person = personService.getPersonById(Integer.parseInt(userToUpdate));
		List<Role> roleList = roleService.getAllRoles();
		model.addObject("person", person);
		model.addObject("roleList", roleList);
		model.setViewName("updateuserpage");
		return model;
	}

	@RequestMapping("/confirm_user_update")
	public ModelAndView confirmUpdateUser(@RequestParam int person_id, @RequestParam String person_name,
			@RequestParam String person_patronymic, @RequestParam String person_last_name,
			@RequestParam String person_email, @RequestParam String person_telephone_number,
			@RequestParam String person_login, @RequestParam String person_password, @RequestParam String person_role) {
		
		personService.saveOrUpdateUser(person_id, person_name, person_patronymic, person_last_name, person_email,
				person_telephone_number, person_login, person_password, person_role);
		return userList();
	}

}
