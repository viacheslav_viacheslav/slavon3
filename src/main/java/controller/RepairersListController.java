package controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import service.PersonService;
import service.SpecializationService;

@Controller
public class RepairersListController {
	@Autowired
	private PersonService personService;

	public PersonService getPersonService() {
		return personService;
	}

	public void setPersonService(PersonService personService) {
		this.personService = personService;
	}
	
	@Autowired
	private SpecializationService specializationService;
	
	
	public SpecializationService getSpecializationService() {
		return specializationService;
	}

	public void setSpecializationService(SpecializationService specializationService) {
		this.specializationService = specializationService;
	}

	@RequestMapping("/repairers")
	public ModelAndView repairersList(){
		ModelAndView model = new ModelAndView();
		model.addObject("repairers", personService.getRepairers());
		model.addObject("specializationSet", specializationService.getAllSpecialization());
		model.setViewName("repairerslistpage");	
		
		return model;
		
	}
}
