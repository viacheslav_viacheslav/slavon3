package controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;



@Controller
public class IndexPageController {

	
	@RequestMapping(value = "/")
	public String index(){
		return "index2";
	}
	
	@RequestMapping(value = "/403")	
	public String notAuthorized() {
		return "index2";
	}
}
