package controller;

import javax.faces.bean.*;
import javax.faces.context.FacesContext;

import dao.PersonDao;
import org.springframework.stereotype.Component;
import service.PersonService;

/**
 * Created by varianytsia on 09.12.17.
 */
@Component
@ManagedBean(name = "adminPageController",eager = true)
@RequestScoped
public class AdminPageController{

@ManagedProperty(value = "#{personService}")
  PersonService personService;

public String getUserName(){
String userLogin = FacesContext.getCurrentInstance().getExternalContext().getRemoteUser();
return personService.getPersonByLogin(userLogin).getFirstName();
}

public String navigateToOrderList(){
    return "/pages/orderListPage?faces-redirect=true";
}

public PersonService getPersonService() {
        return personService;
    }

    public void setPersonService(PersonService personService) {
        this.personService = personService;
    }
}
