package controller;

import entity.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import service.CarService;
import service.CommentService;
import service.OrderService;
import service.OrderStatusService;
import service.PersonService;

import javax.annotation.PostConstruct;
import javax.faces.bean.*;
import javax.faces.context.FacesContext;
import java.util.List;

@Component
@ManagedBean(name = "orderListController")
@ViewScoped
public class OrderListController {

	@ManagedProperty(value = "#{personService}")
	private PersonService personService;

	public PersonService getPersonService() {
		return personService;
	}

	public void setPersonService(PersonService personService) {
		this.personService = personService;
	}

	@ManagedProperty(value = "#{carService}")
	private CarService carService;

	public CarService getCarService() {
		return carService;
	}

	public void setCarService(CarService carService) {
		this.carService = carService;
	}

	@ManagedProperty(value = "#{orderStatusService}")
	private OrderStatusService orderStatusService;

	public OrderStatusService getOrderStatusService() {
		return orderStatusService;
	}

	public void setOrderStatusService(OrderStatusService orderStatusService) {
		this.orderStatusService = orderStatusService;
	}

	@ManagedProperty(value = "#{commentService}")
	private CommentService commentService;

	public CommentService getCommentService() {
		return commentService;
	}

	public void setCommentService(CommentService commentService) {
		this.commentService = commentService;
	}

	@ManagedProperty(value = "#{orderService}")
	private OrderService orderService;

	public OrderService getOrderService() {
		return orderService;
	}

	public void setOrderService(OrderService orderService) {
		this.orderService = orderService;
	}

	@ManagedProperty(value = "#{updateOrderController}")
	UpdateOrderController updateOrderController;

	public UpdateOrderController getUpdateOrderController() {
		return updateOrderController;
	}

	public void setUpdateOrderController(UpdateOrderController updateOrderController) {
		this.updateOrderController = updateOrderController;
	}

	@PostConstruct
	public void init(){
		Order order = new Order();
		order.setLastComment(new Comment());
		order.setCar(new Car());
		order.setOrderStatus(new OrderStatus());
	this.targetOrder = order;
		}

	private Order targetOrder;

	private List<Order> orderList;

	private Person client;

	private Person repairer;

	private Order order;

	private List<Car> cars;

	private List<Person> clients;

	private List<OrderStatus> orderStatusList;

	private List<Person> repairers;

	int orderIdToEdit;



	public Order getTargetOrder() {
		return targetOrder;
	}

	public void setTargetOrder(Order targetOrder) {
		this.targetOrder = targetOrder;
	}

	public List<Order> getOrderList() {
		List<Order>orders = orderService.getAllOrders();
		for (Order order : orders){
			if(order.getOrderId()==orderIdToEdit){
				order.setEditable(true);
			}
		}
        return orders;
	}

	public Person getClient(int id){
		return personService.getPersonById(id);
	}

    public Person getRepairer(int id) {
        return personService.getPersonById(id);
    }

	public List<Car> getCars() {
		return carService.getAllCars();
	}

	public List<Person> getClients() {
		return personService.getClients();
	}

	public List<OrderStatus> getOrderStatusList() {
		return orderStatusService.getAllOrderStatuses();
	}

	public List<Person> getRepairers() {
		return personService.getRepairers();
	}

	public String addOrder() {
		return "/pages/addOrderPage.xhtml?faces-redirect=true";
	}

	public String confirmAddOrder(){
		this.targetOrder.getLastComment().setPerson(updateOrderController.detectCurrentUser());
		orderService.insertOrUpdateOrder(this.targetOrder);
		return "/pages/orderListPage?faces-redirect=true";
	}


}
