package controller;

import entity.Order;
import entity.Person;
import org.springframework.stereotype.Component;
import service.OrderService;
import service.PersonService;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;


@Component
@ManagedBean (name = "updateOrderController")
@SessionScoped
public class UpdateOrderController {

    @ManagedProperty(value = "#{personService}")
    private PersonService personService;

    public PersonService getPersonService() {
        return personService;
    }

    public void setPersonService(PersonService personService) {
        this.personService = personService;
    }

    @ManagedProperty(value = "#{orderService}")
    private OrderService orderService;

    public OrderService getOrderService() {
        return orderService;
    }

    public void setOrderService(OrderService orderService) {
        this.orderService = orderService;
    }

//    @ManagedProperty(value = "#{orderListController}")
//    OrderListController orderListController;
//
//    public OrderListController getOrderListController() {
//        return orderListController;
//    }
//
//    public void setOrderListController(OrderListController orderListController) {
//        this.orderListController = orderListController;
//    }

    private Person client;

    private Person repairer;

    private Order order;

    private Order targetOrder;

    public Order getTargetOrder() {
        return targetOrder;
    }

    public void setTargetOrder(Order targetOrder) {
        this.targetOrder = targetOrder;
    }

    public String editAction(Order order){
        targetOrder=order;
        targetOrder.getLastComment().setCommentText("");
        return "/pages/updateOrderPage.xhtml?faces-redirect=true";
    }

    public Person getClient() {
        return client;
    }

    public void setClient(Person client) {
        this.client = client;
    }

    public Person getRepairer() {
        return repairer;
    }

    public void setRepairer(Person repairer) {
        this.repairer = repairer;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public String confirmUpdateOrder(){
        this.targetOrder.getLastComment().setPerson(detectCurrentUser());
        orderService.insertOrUpdateOrder(this.targetOrder);
        return "/pages/orderListPage?faces-redirect=true";
    }

    public Person detectCurrentUser(){
        String login = FacesContext.getCurrentInstance().getExternalContext().getRemoteUser();
        Person person = personService.getPersonByLogin(login);
        return person;
    }

    public String cancelUpdateAction(){
        return "/pages/orderListPage?faces-redirect=true";
    }
}
