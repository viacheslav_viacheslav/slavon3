package dao;

import java.util.List;

import entity.Person;

public interface PersonDao {

	void insertOrUpdatePerson(Person person);

	Person getPersonById(int id);

	Person getPersonByLogin(String name);

	void deletePerson(Person person);

	List<Person> getAllPeople();

	List<Person> getClients();

	List<Person> getRepairers();

}
