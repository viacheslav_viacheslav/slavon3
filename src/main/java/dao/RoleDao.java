package dao;

import java.util.List;

import entity.Role;

public interface RoleDao {
	void insertOrUpdateRole(Role role);

	Role getRoleById(int id);

	void deleteRole(Role role);

	List<Role> getAllRoles();

}
