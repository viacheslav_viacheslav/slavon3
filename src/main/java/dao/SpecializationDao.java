package dao;

import java.util.List;

import entity.Specialization;

public interface SpecializationDao {
	void insertOrUpdateSpecialization(Specialization specialization);

	Specialization getSpecializationById(int id);

	void deleteSpecialization(Specialization specialization);

	List<Specialization> getAllSpecialization();
}
