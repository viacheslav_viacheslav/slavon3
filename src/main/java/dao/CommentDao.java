package dao;

import java.util.List;

import entity.Comment;

public interface CommentDao {
	
	int insertComment(Comment comment);

	Comment getCommentById(int id);

	List<Comment> getAllComments();

}
