package dao;

import java.util.List;

import entity.OrderStatus;

public interface OrderStatusDao {

	void insertOrUpdateOrderStatus(OrderStatus orderStatus);

	OrderStatus getOrderStatusById(int id);

	void deleteOrderStatus(OrderStatus orderStatus);

	List<OrderStatus> getAllOrderStatuses();

}
