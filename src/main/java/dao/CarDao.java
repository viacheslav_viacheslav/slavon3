package dao;

import java.util.List;

import entity.Car;

public interface CarDao {

	void insertOrUpdateCar(Car car);

	Car getCarById(int id);

	void deleteCar(Car car);

	List<Car> getAllCars();

}
