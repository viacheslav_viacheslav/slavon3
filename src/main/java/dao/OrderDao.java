package dao;

import java.util.List;

import entity.Order;


public interface OrderDao {
	
	void insertOrUpdateOrder(Order order);

	Order getOrderById(int id);

	void deleteOrder(Order order);

	List<Order> getAllOrders();

}
