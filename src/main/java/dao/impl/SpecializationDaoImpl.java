package dao.impl;

import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import entity.Specialization;
import dao.SpecializationDao;

public class SpecializationDaoImpl implements SpecializationDao {

	private SessionFactory sessionFactory;

	public SpecializationDaoImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}


	@Override
	public void insertOrUpdateSpecialization(Specialization specialization) {
		Session session = sessionFactory.getCurrentSession();
		session.saveOrUpdate(specialization);
	}


	@Override
	public Specialization getSpecializationById(int id) {
		Session session = sessionFactory.getCurrentSession();
		Specialization specialization = (Specialization) session.get(Specialization.class, new Integer(id));
		return specialization;
	}

	
	@Override
	public void deleteSpecialization(Specialization specialization) {
		Session session = sessionFactory.getCurrentSession();
		session.delete(specialization);
	}


	@Override
	public List<Specialization> getAllSpecialization() {
		Session session = sessionFactory.getCurrentSession();
		List<Specialization> specializationList = session.createQuery("from SPECIALIZATION").list();
		return specializationList;
	}

}
