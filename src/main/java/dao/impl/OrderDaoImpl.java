package dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import entity.Order;
import dao.OrderDao;

public class OrderDaoImpl implements OrderDao {
	private SessionFactory sessionFactory;

	public OrderDaoImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}


	@Override
	public void insertOrUpdateOrder(Order order) {
		Session session = sessionFactory.getCurrentSession();
//		session.saveOrUpdate(order);
		session.merge(order);
	}


	@Override
	public Order getOrderById(int id) {
		Session session = sessionFactory.getCurrentSession();
		Order order = (Order) session.get(Order.class, new Integer(id));
		return order;
	}


	@Override
	public void deleteOrder(Order order) {
		Session session = sessionFactory.getCurrentSession();
		session.delete(order);
	}


	@Override
	public List<Order> getAllOrders() {
		Session session = sessionFactory.getCurrentSession();
		List<Order> orderList = session.createQuery("from ORDERS").list();
		return orderList;
	}

}
