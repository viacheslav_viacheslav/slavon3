package dao.impl;

import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import entity.Car;
import dao.CarDao;


public class CarDaoImpl implements CarDao {
	private SessionFactory sessionFactory;

	public CarDaoImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}


	@Override
	public void insertOrUpdateCar(Car car) {
		Session session = sessionFactory.getCurrentSession();
		session.saveOrUpdate(car);
	}


	@Override
	public Car getCarById(int id) {
		Session session = sessionFactory.getCurrentSession();
		Car car = (Car) session.get(Car.class, new Integer(id));
		return car;
	}


	@Override
	public void deleteCar(Car car) {
		Session session = sessionFactory.getCurrentSession();
		session.delete(car);
	}


	@Override
	public List<Car> getAllCars() {
		Session session = sessionFactory.getCurrentSession();
		List<Car> carList = session.createQuery("from CAR").list();
		return carList;
	}

}
