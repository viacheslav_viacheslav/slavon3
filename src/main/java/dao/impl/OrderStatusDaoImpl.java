package dao.impl;

import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import entity.OrderStatus;
import dao.OrderStatusDao;

public class OrderStatusDaoImpl implements OrderStatusDao {
	private SessionFactory sessionFactory;

	public OrderStatusDaoImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}


	@Override
	public void insertOrUpdateOrderStatus(OrderStatus orderStatus) {
		Session session = sessionFactory.getCurrentSession();
		session.saveOrUpdate(orderStatus);
	}


	@Override
	public OrderStatus getOrderStatusById(int id) {
		Session session = sessionFactory.getCurrentSession();
		OrderStatus orderStatus = (OrderStatus) session.get(OrderStatus.class, new Integer(id));
		return orderStatus;
	}


	@Override
	public void deleteOrderStatus(OrderStatus orderStatus) {
		Session session = sessionFactory.getCurrentSession();
		session.delete(orderStatus);
	}


	@Override
	public List<OrderStatus> getAllOrderStatuses() {
		Session session = sessionFactory.getCurrentSession();
		List<OrderStatus> orderStatusList = session.createQuery("from ORDER_STATUS").list();
		return orderStatusList;
	}
}
