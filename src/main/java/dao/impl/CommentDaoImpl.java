package dao.impl;

import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import entity.Comment;
import dao.CommentDao;

public class CommentDaoImpl implements CommentDao {
	private SessionFactory sessionFactory;

	public CommentDaoImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}


	@Override
	public int insertComment(Comment comment) {
		Session session = sessionFactory.getCurrentSession();
		int w = (int) session.save(comment);
		return w;
	}
	

	@Override
	public Comment getCommentById(int id) {
		Session session = sessionFactory.getCurrentSession();
		Comment comment = (Comment) session.get(Comment.class, new Integer(id));
		return comment;
	}
	

	@Override
	public List<Comment> getAllComments() {
		Session session = sessionFactory.getCurrentSession();
		List<Comment> commentList = session.createQuery("from COMMENT").list();
		return commentList;
	}

}
