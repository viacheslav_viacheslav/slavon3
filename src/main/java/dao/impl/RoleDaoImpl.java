package dao.impl;

import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import entity.Role;
import dao.RoleDao;

public class RoleDaoImpl implements RoleDao {
	private SessionFactory sessionFactory;

	public RoleDaoImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}


	@Override
	public void insertOrUpdateRole(Role role) {
		Session session = sessionFactory.getCurrentSession();
		session.saveOrUpdate(role);
	}


	@Override
	public Role getRoleById(int id) {
		Session session = sessionFactory.getCurrentSession();
		Role role = (Role) session.get(Role.class, new Integer(id));
		return role;
	}


	@Override
	public void deleteRole(Role role) {
		Session session = sessionFactory.getCurrentSession();
		session.delete(role);
	}


	@Override
	public List<Role> getAllRoles() {
		Session session = sessionFactory.getCurrentSession();
		List<Role> roleList = session.createQuery("from ROLE").list();
		return roleList;

	}

}
