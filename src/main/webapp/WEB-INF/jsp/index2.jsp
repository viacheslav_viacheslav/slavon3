<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<html>
<head>
<meta charset="UTF-8">
<title>Car Service</title>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<link href="/spring_2/style/style.css" type="text/css" rel="stylesheet"  media="screen"/>
</head>
<body class="face">


	<h1 align="center" class="page_title">WELCOME TO SERVICE STATION</h1>



	<div align="center" style="vertical-align: top">
		<form action="j_spring_security_check" method="post">
			Login:<input type="text" name="j_username" value="admin"><br>
			Password:<input type="text" name="j_password" value="admin"><br>
			<input type="submit" value="Login">
			<input type="hidden" name="${_csrf.parameterName}"value="${_csrf.token}"/>
		</form>
	</div>



	<div class="footer" style="vertical-align: top">
		<%@include file="footerpage.jsp"%>
	</div>
	
</body>
</html>