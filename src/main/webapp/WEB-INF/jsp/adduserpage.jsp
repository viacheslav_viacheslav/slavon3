<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Add user</title>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<link href="/servlets_jsp2/style/style.css" type="text/css"
	rel="stylesheet" media="screen" />
</head>
<body>

	<div class="header">
		<%@include file="header.jsp"%>
	</div>
	</div>
	<p>
	<h2 align="center" class="page_title">Add user:</h2>
	</p>
	<table style="width: 70%">
		<tr>
			<th>Name</th>
			<th>Patronymic</th>
			<th>Last Name</th>
			<th>E-mail</th>
			<th>Telephone number</th>
			<th>Login</th>
			<th>Password</th>
			<th>Role</th>
		</tr>
		<tr>
			<form action="confirm_user_add" method="post">
				<td><input type="text" name="person_name"><br></td>
				<td><input type="text" name="person_patronymic" /><br></td>
				<td><input type="text" name="person_last_name" /><br></td>
				<td><input type="text" name="person_email" /><br></td>
				<td><input type="text" name="person_telephone_number" /><br></td>
				<td><input type="text" name="person_login" required /><br></td>
				<td><input type="text" name="person_password" /><br></td>
				<td><select name="person_role">
						<c:forEach items="${roleList}" var="role">
							<option value="${role.getRoleId()}">${role.getRoleName()}</option>
						</c:forEach>
				</select></td>
				<td><input type="submit" value="submit">
				<input type="hidden" name="${_csrf.parameterName}"value="${_csrf.token}"/>
			</form>
			<form action="cancel_user_update" method="get">
				<input type="submit" value="cancel">
			</form>
		</tr>
	</table>
	<div class="footer">
		<%@include file="/WEB-INF/jsp/footerpage.jsp"%>
	</div>

</body>
</html>