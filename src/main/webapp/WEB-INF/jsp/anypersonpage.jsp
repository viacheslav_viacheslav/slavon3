<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="bean.Person"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Anyperson's page</title>
<link href="/servlets_jsp2/style/style.css" type="text/css"
	rel="stylesheet" media="screen" />
</head>
<body>
	<div class="header">
		<%@include file="/WEB-INF/jsp/header.jsp"%>
	</div>
	<%
		Person person = (Person) session.getAttribute("currentPerson");
	%>
	<h1 class="greetings">
		Hello,
		<%
		String personName = person.getFirstName();
		if (personName == null) {
			out.print("Anonymous");
		} else
			out.print(personName);
	%>
		<%
			String personLastName = person.getPersonLastName();
			if (personLastName == null) {
				out.print("Anonymous");
			} else
				out.print(personLastName);
		%>
	</h1>
	<div class="footer">
		<%@include file="/WEB-INF/jsp/footerpage.jsp"%>
	</div>

</body>
</html>