<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Orders</title>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<link href="/servlets_jsp2/style/style.css" type="text/css" rel="stylesheet"  media="screen"/>
</head>
<body>
	<div class="header">
		<%@include file="header.jsp"%>
	</div>
	<p>
	<h2 align="center" class="page_title">Update order:</h2>
	</p>
	<table style="width: 100%">
		<tr>
			<th>ID</th>
			<th>Car</th>
			<th>Client</th>
			<th>Order Status</th>
			<th>Repairer</th>
			<th>Order Price</th>
			<th>Comments</th>
		</tr>
		<c:set var="order" scope="request" value="${order}" />
		<tr>
		<form action="confirm_order_update" method="post">
			<td><c:out value="${order.getOrderId()}" /></td>
			<td><select name="car_id">
					<c:forEach items="${carList}" var="car">
						<c:choose>
							<c:when test="${order.getCar().getCarId()==car.getCarId()}">
								<option selected="selected" value="${car.getCarId()}">${car.getCarNumber()}</option>
							</c:when>
							<c:otherwise>
								<option value="${car.getCarId()}">${car.getCarNumber()}</option>
							</c:otherwise>
						</c:choose>
					</c:forEach>
			</select></td>
			<c:choose>
				<c:when test="${order.getClientId()==0}">
					<td><select name="client_id">
							<option selected></option>
							<c:forEach items="${clientsList}" var="client">
								<option value="${client.getPersonId()}">${client.getFirstName()}
									${client.getPersonLastName()}</option>
							</c:forEach>
					</select></td>
				</c:when>
				<c:otherwise>
					<td><select name="client_id">
							<c:forEach items="${clientsList}" var="client">
								<c:choose>
									<c:when test="${order.getClientId()==client.getPersonId()}">
										<option selected="selected" value="${client.getPersonId()}">${client.getFirstName()}
											${client.getPersonLastName()}</option>
									</c:when>
									<c:otherwise>
										<option value="${client.getPersonId()}">${client.getFirstName()}
											${client.getPersonLastName()}</option>
									</c:otherwise>
								</c:choose>
							</c:forEach>
					</select></td>
				</c:otherwise>
			</c:choose>
			<td><select name="order_status_id">
					<c:forEach items="${orderStatusesList}"
						var="orderStatus">
						<c:choose>
							<c:when
								test="${order.getOrderStatus().getOrderStatusId()==orderStatus.getOrderStatusId()}">
								<option selected="selected"
									value="${orderStatus.getOrderStatusId()}">${orderStatus.getOrderStatus()}</option>
							</c:when>
							<c:otherwise>
								<option value="${orderStatus.getOrderStatusId()}">${orderStatus.getOrderStatus()}</option>
							</c:otherwise>
						</c:choose>
					</c:forEach>
			</select></td>
			<c:choose>
				<c:when test="${order.getRepairerId()==0}">
					<td><select name="repairer_id">
							<option selected></option>
							<c:forEach items="${repairersList}" var="repairer">
								<option value="${repairer.getPersonId()}">${repairer.getFirstName()}
									${repairer.getPersonLastName()}</option>
							</c:forEach>
					</select></td>
				</c:when>
				<c:otherwise>
					<td><select name="repairer_id">
							<c:forEach items="${repairersList}" var="repairer">
								<c:choose>
									<c:when test="${order.getRepairerId()==repairer.getPersonId()}">
										<option selected="selected" value="${repairer.getPersonId()}">${repairer.getFirstName()}
											${repairer.getPersonLastName()}</option>
									</c:when>
									<c:otherwise>
										<option value="${repairer.getPersonId()}">${repairer.getFirstName()}
											${repairer.getPersonLastName()}</option>
									</c:otherwise>
								</c:choose>
							</c:forEach>
					</select></td>
				</c:otherwise>
			</c:choose>
			<td><input type="text" name="order_price"
				value="${order.getOrderPrice()}"></td>
			<td width="30%" align="left">
			<input type="text"
				name="order_comment" required></td>
			<td>				
					<input type="submit" value="confirm">
					<input type="hidden" name="${_csrf.parameterName}"value="${_csrf.token}"/> 
					<input type="hidden"
						name="submitPerson" value="${pageContext.request.userPrincipal.name}" />
					<input type="hidden"
						name="orderToUpdate" value="${order.getOrderId()}" />
				</form>
				<form action="orders" method="get">
					<input type="submit" value="cancel">
				</form>
			</td>
		</tr>
	</table>
	<div class="footer">
		<%@include file="/WEB-INF/jsp/footerpage.jsp"%>
	</div>
</body>
</html>