<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Error page</title>
<link href="/servlets_jsp2/style/style.css" type="text/css"
	rel="stylesheet" media="screen" />
</head>
<body>
	<div class="header">
		<%@include file="/WEB-INF/jsp/header.jsp"%>
	</div>
	<h1 class="error_message">Something went wrong, please go back and
		try again</h1>
	<p class="error_message">${exception}</p>


	<div class="footer">
		<%@include file="/WEB-INF/jsp/footerpage.jsp"%>
	</div>

</body>
</html>