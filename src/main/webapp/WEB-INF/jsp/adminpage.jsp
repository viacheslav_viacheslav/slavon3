<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="entity.Person"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Start page</title>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<link href="/spring_2/style/style.css" type="text/css"
	rel="stylesheet" media="screen" />
</head>
<body>
	<div class="header">
		<%@include file="header.jsp"%>
	</div>
	
	
	<c:if test="${pageContext.request.userPrincipal.name != null}">
		<h1 class="greetings">
			Welcome : ${pageContext.request.userPrincipal.name}  
		</h1>
	</c:if>
	

	<div class="topspace"></div>
	<sec:authorize access="hasRole('ROLE_ADMINISTRATOR')">
	<form action="orders" method="get">
		<button class="button" type="submit" value="Submit">Go to
			Orders List</button>
	</form>
	</sec:authorize>
	<sec:authorize access="hasAnyRole('ROLE_ADMINISTRATOR','ROLE_REPAIRER')">
	<div class="space"></div>
	<form action="repairers" method="get">
		<button class="button" type="submit" value="Submit">Go to
			Repairers List</button>
	</form>
	</sec:authorize>
	<sec:authorize access="hasRole('ROLE_ADMINISTRATOR')">
	<div class="space"></div>
	<form action="users" method="get">
		<button class="button" type="submit" value="Submit">Go to
			Users List</button>
	</form>
	</sec:authorize>

	<div class="footer">
		<%@include file="/WEB-INF/jsp/footerpage.jsp"%>
	</div>
</body>
</html>