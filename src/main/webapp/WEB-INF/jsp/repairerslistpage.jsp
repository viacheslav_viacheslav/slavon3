<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Repairers</title>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<link href="/servlets_jsp2/style/style.css" type="text/css" rel="stylesheet"  media="screen"/>
</head>
<body>
		<div class = "header">
	<%@include file="header.jsp"%>
	</div>
	<p>
	<h2 align="center" class="page_title">Repairers:</h2>
	</p>
	<table style="width: 100%">
		<tr>
			<th>ID</th>
			<th>Specialization</th>
			<th>Name</th>
			<th>Patronymic</th>
			<th>Last Name</th>
			<th>Telephone number</th>
			<th>Order Number</th>
		</tr>
		<c:forEach items="${repairers}" var="person">
			<tr>
				<td><c:out value="${person.getPersonId()}" /></td>
				<td><c:forEach items="${specializationSet}"
						var="specialization">
						<c:choose>
							<c:when
								test="${person.getSpecializationSet().contains(specialization)}">
								<p>
									<c:out value="${specialization.getSpecialization()}" />
								</p>
							</c:when>
						</c:choose>
					</c:forEach></td>
				<td><c:out value="${person.getFirstName()}" /></td>
				<td><c:out value="${person.getPersonPatronymic()}" /></td>
				<td><c:out value="${person.getPersonLastName()}" /></td>
				<td><c:out value="${person.getPersonTelephoneNumber()}" /></td>
				<td><c:forEach items="${person.getOrderSet()}" var="order">
						<c:choose>
							<c:when test="${order.getOrderId()>=1}">
								<p>[${order.getOrderId()}]</p>
							</c:when>
							<c:otherwise>
							<p>NO ORDERS</p>
								<c:out value="NO ORDERS" />
							</c:otherwise>
						</c:choose>
					</c:forEach></td>
		</c:forEach>
		</tr>
	</table>
	<div class="footer">
		<%@include file="footerpage.jsp"%>
	</div>
</body>
</html>