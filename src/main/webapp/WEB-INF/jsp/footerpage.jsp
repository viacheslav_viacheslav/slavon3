<%@page import="java.text.SimpleDateFormat"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="/spring_2/style/style.css" type="text/css" rel="stylesheet"  media="screen"/>
</head>
<body>
	<div class="thefooter">
		<p>Slavon corporation � 2017</p>
		<p>
			<%
				String pattern = "yyyy-MM-dd";
				SimpleDateFormat sdf = new SimpleDateFormat(pattern);
				out.print(sdf.format(new java.util.Date()));
			%>
		</p>
	</div>

</body>
</html>