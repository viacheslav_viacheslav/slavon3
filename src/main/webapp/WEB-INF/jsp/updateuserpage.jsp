<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Orders</title>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<link href="/servlets_jsp2/style/style.css" type="text/css" rel="stylesheet"  media="screen"/>
</head>
<body>
	<div class="header">
		<%@include file="header.jsp"%>
	</div>
	<p>
	<h2 align="center" class="page_title">Update user:</h2>
	</p>
	<table style="width: 90%">
		<tr>
			<th>ID</th>
			<th>Name</th>
			<th>Patronymic</th>
			<th>Last Name</th>
			<th>E-mail</th>
			<th>Telephone number</th>
			<th>Login</th>
			<th>Password</th>
			<th>Role</th>
		</tr>
<c:set var="person" scope="request" value="${person}"/>
		<tr>
		<form action="confirm_user_update" method="post">
			<td><c:out value="${person.getPersonId()}" /></td>
			<input type="hidden" name="person_id" value="${person.getPersonId()}">
			<td><input type="text" name="person_name" value="${person.getFirstName()}"><br></td>
			<td><input type="text" name="person_patronymic" value="${person.getPersonPatronymic()}"/><br></td>
			<td><input type="text" name="person_last_name" value="${person.getPersonLastName()}" /><br></td>
			<td><input type="text" name="person_email" value="${person.getPersonEmail()}" /><br></td>
			<td><input type="text" name="person_telephone_number" value="${person.getPersonTelephoneNumber()}" /><br></td>
			<td><input type="text" name="person_login" value="${person.getPersonLogin()}" required/><br></td>
			<td><input type="text" name="person_password" value="${person.getPersonPassword()}" /><br></td>
					<td><select name="person_role">
			<c:forEach items="${roleList}" var="role">		
                          <c:choose>
							<c:when test="${role.getRoleId()==person.getPersonRole().getRoleId()}">
								<option selected="selected" value ="${role.getRoleId()}">${role.getRoleName()}</option>
							</c:when>
							<c:otherwise>
								<option value ="${role.getRoleId()}">${role.getRoleName()}</option>
							</c:otherwise>
						</c:choose>		
		</c:forEach>
		</select></td>
        <td><input type="submit" value = "submit">
		<input type="hidden" name="person_id" value="${person.getPersonId()}" />
		<input type="hidden" name="${_csrf.parameterName}"value="${_csrf.token}"/>
		</form>
		<form action="cancel_user_update" method="get">
		<input type="submit" value = "cancel">
		</form>
		</tr>
	</table>
	<div class="footer">
		<%@include file="/WEB-INF/jsp/footerpage.jsp"%>
	</div>

</body>
</html>