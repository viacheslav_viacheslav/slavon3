<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Orders</title>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<link href="/servlets_jsp2/style/style.css" type="text/css"
	rel="stylesheet" media="screen" />
</head>
<body>
	<div class="header">
		<%@include file="header.jsp"%>
	</div>
	<p>
	<h2 align="center" class="page_title">Orders:</h2>
	</p>
	<!--<c:set var="currentPerson" scope="session"
		value="${sessionScope.currentPerson}" /> -->
	<p>
	<div align="center" style="vertical-align: top">
		<sec:authorize access="hasRole('ROLE_ADMINISTRATOR')">
			<form action="add_order" method="get">
				<input class="home_button" type="submit" value="add new order">
			</form>
		</sec:authorize>
	</div>
	</p>
	<table style="width: 100%">
		<tr>
			<th>ID</th>
			<th>Car</th>
			<th>Client Name</th>
			<th>Client Last Name</th>
			<th>Order Status</th>
			<th>Repairer Name</th>
			<th>Repairer Last Name</th>
			<th>Order Price</th>
			<th>Comments</th>
		</tr>
			<c:forEach items="${orderList}" var="order">
			<tr>
				<td><c:out value="${order.getOrderId()}" /></td>
				<td><c:out
						value="${carMap.get(order.getCar().getCarId()).getCarNumber()}" /></td>
				<td><c:choose>
						<c:when test="${order.getClientId()==0}">
							<c:out value="" />
						</c:when>
						<c:otherwise>
							<c:out
								value="${personMap.get(order.getClientId()).getFirstName()}" />
						</c:otherwise>
					</c:choose></td>
				<td><c:choose>
						<c:when test="${order.getClientId()==0}">
							<c:out value="" />
						</c:when>
						<c:otherwise>
							<c:out
								value="${personMap.get(order.getClientId()).getPersonLastName()}" />
						</c:otherwise>
					</c:choose></td>
				<td><c:out
						value="${orderStatusMap.get(order.getOrderStatus().getOrderStatusId()).getOrderStatus()}" /></td>
				<td><c:choose>
						<c:when test="${order.getRepairerId()==0}">
							<c:out value="" />
						</c:when>
						<c:otherwise>
							<c:out
								value="${personMap.get(order.getRepairerId()).getFirstName()}" />
						</c:otherwise>
					</c:choose></td>
				<td><c:choose>
						<c:when test="${order.getRepairerId()==0}">
							<c:out value="" />
						</c:when>
						<c:otherwise>
							<c:out
								value="${personMap.get(order.getRepairerId()).getPersonLastName()}" />
						</c:otherwise>
					</c:choose></td>
				<td><c:out value="${order.getOrderPrice()}" /></td>
				<td style="width: 30%; text-align: left; word-wrap: break-word;">
					<c:forEach items="${order.getCommentList()}" var="comment1">
						
						<c:set var="comment"
							value="${commentsMap.get(comment1.getCommentId())}" />
						<c:set var="commentPerson"
							value="${personMap.get(comment1.getPerson().getPersonId())}" />

						<c:out
							value=" ${commentPerson.getFirstName()}  ${commentPerson.getPersonLastName()}  ${comment1.getCommentDateTime()}: ${comment1.getCommentText()}" />
						<br> 
					</c:forEach>
				</td>
				<td><sec:authorize access="hasRole('ROLE_ADMINISTRATOR')">
						<form action="update_order" method="get">
							<input type="submit" value="update"> <input type="hidden"
								name="orderToUpdate" value="${order.getOrderId()}" />
						</form>
					</sec:authorize></td>
			</tr>
		</c:forEach>
	</table>
	<div class="footer">
		<%@include file="/WEB-INF/jsp/footerpage.jsp"%>
	</div>
</body>
</html>