
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Add order</title>

<link href="/servlets_jsp2/style/style.css" type="text/css"
	rel="stylesheet" media="screen" />

</head>
<body>
	<div class="header">
		<%@include file="header.jsp"%>
	</div>
	<p>
	<h2 align="center" class="page_title">Add order:</h2>
	</p>
	<div class="table">
		<table style="width: 100%">

			<tr>

				<th>Car</th>
				<th>Client</th>
				<th>Order Status</th>
				<th>Repairer</th>
				<th>Order Price</th>
				<th>Comments</th>

			</tr>
			<tr>
				<form action="confirm_order_add" method="post">

					<td><select name="car_id" required='required'>
							<option selected disabled value=''>Please select</option>
							<c:forEach items="${carList}" var="car">
								<option value="${car.getCarId()}">${car.getCarNumber()}</option>
							</c:forEach>
					</select></td>
					<td><select name="client_id" required="required">
							<option selected disabled value=''>Please select</option>
							<c:forEach items="${clientList}" var="client">
								<option value="${client.getPersonId()}">${client.getFirstName()}
									${client.getPersonLastName()}</option>
							</c:forEach>
					</select></td>

					<td><select name="order_status_id" required="required">
							<option selected disabled value=''>Please select</option>
							<c:forEach items="${orderStatusList}"
								var="orderStatus">
								<option value="${orderStatus.getOrderStatusId()}">${orderStatus.getOrderStatus()}</option>
							</c:forEach>
					</select></td>

					<td><select name="repairer_id" required="required">
							<option selected disabled>Please select</option>
							<c:forEach items="${repairerList}" var="repairer">
								<option value="${repairer.getPersonId()}">${repairer.getFirstName()}
									${repairer.getPersonLastName()}</option>
							</c:forEach>
					</select></td>
					<td><input type="text" name="order_price"></td>

					<td width="30%" align="left"><input type="text"
						name="order_comment" required></td>
					<td><input type="submit" value="confirm">
					<input type="hidden"
						name="submitPerson" value="${pageContext.request.userPrincipal.name}" />
						<input type="hidden" name="${_csrf.parameterName}"value="${_csrf.token}"/>
				</form>

				<form action="orders" method="get">
					<input type="submit" value="cancel">
				</form>
				</td>
			</tr>



		</table>
	</div>
	<div class="footer">
		<%@include file="/WEB-INF/jsp/footerpage.jsp"%>
	</div>
</body>
</html>