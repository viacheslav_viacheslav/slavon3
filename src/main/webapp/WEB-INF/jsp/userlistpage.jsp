<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Users</title>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<link href="/spring_2/style/style.css" type="text/css" rel="stylesheet"
	media="screen" />
<%@ page import="java.util.ArrayList, entity.Person"%>
</head>
<body>
	<div class="header">
		<%@include file="header.jsp"%>
	</div>
	<p>
	<h2 align="center" class="page_title">Users:</h2>
	</p>

	<!--<c:set var="currentPerson" scope="session"
		value="${sessionScope.currentPerson}" /> -->
	<p>
	<div align="center" style="vertical-align: top">
		<sec:authorize access="hasRole('ROLE_ADMINISTRATOR')">
			<form action="add_user" method="get">
				<input class="home_button" type="submit" value="add new user">
			</form>
		</sec:authorize>
	</div>
	</p>
	<table style="width: 100%">
		<tr>
			<th>ID</th>
			<th>Name</th>
			<th>Patronymic</th>
			<th>Last Name</th>
			<th>E-mail</th>
			<th>Telephone number</th>
			<th>Login</th>
			<th>Password</th>
			<th>Role</th>
		</tr>
		<c:forEach items="${personList}" var="person">
			<tr>
				<td><c:out value="${person.getPersonId()}" /></td>
				<td><c:out value="${person.getFirstName()}" /></td>
				<td><c:out value="${person.getPersonPatronymic()}" /></td>
				<td><c:out value="${person.getPersonLastName()}" /></td>
				<td><c:out value="${person.getPersonEmail()}" /></td>
				<td><c:out value="${person.getPersonTelephoneNumber()}" /></td>
				<td><c:out value="${person.getPersonLogin()}" /></td>
				<td><c:out value="${person.getPersonPassword()}" /></td>
				<td><c:out value="${person.getPersonRole().getRoleName()}" /></td>
				 	<sec:authorize access="hasRole('ROLE_ADMINISTRATOR')">
					<td>
						<form action="delete_user" method="post">
							<c:choose>
								<c:when
									test="${person.getPersonLogin()==pageContext.request.userPrincipal.name}">
									<input type="submit" value="delete" disabled>
								</c:when>
								<c:otherwise>
									<input type="submit" value="delete" onclick="return confirm('are you sure?')">
									<input type="hidden"  name="${_csrf.parameterName}"   value="${_csrf.token}"/>
								</c:otherwise>
							</c:choose>
							<input type="hidden" name="userToDelete"
								value="${person.getPersonId()}" />
						</form>
						<form action="update_user" method="post">
							<input type="submit" value="update"> <input type="hidden"
								name="userToUpdate" value="${person.getPersonId()}" />
								<input type="hidden"  name="${_csrf.parameterName}"   value="${_csrf.token}"/>
						</form>
					</td>
				</sec:authorize> 
			</tr>
		</c:forEach>
	</table>
	<div class="footer">
		<%@include file="footerpage.jsp"%>
	</div>
</body>
</html>